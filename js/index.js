$(function() {
    $("[data-toggle='tooltip']").tooltip(); //Se evalua el jquery y busca los elemento con atributos data-toogle con valor tooltip. se aplica la funcion tooltip
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({ //clase carousel. funcion carousel
        interval: 1950
    });
    $('#contacto').on('show.bs.modal', function(e) {
        console.log('El modal se está mostrando');
        $('#contactorsv').removeClass('btn-success');
        $('#contactorsv').addClass('btn-primary');
        $('#contactorsv').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function(e) {
        console.log('El modal se mostro');
    });
    $('#contacto').on('hide.bs.modal', function(e) {
        console.log('El modal se oculta');
    });
    $('#contacto').on('hidden.bs.modal', function(e) {
        console.log('El modal se oculto');
        $('#contactorsv').prop('disabled', false);
    });

});